#!/bin/bash

DAY=$(date "+%u")
GUID=$(getent group | grep "^admin" | awk -F ":" '{print $3}')

if [ -z "$(id $PAM_USER | grep $GUID)" ];
    then
        if [ $DAY -gt 5 ];
                then exit 1
                else exit 0
        fi
fi