# Otus_task10

Пользователи и группы. Авторизация и аутентификация
Sudo, Umask, Sgid, Suid, Sapabilities, PAM ,ACL, PolicyKit, AppArmor

Домашнее задание
PAM
1. Запретить всем пользователям, кроме группы admin логин в выходные(суббота и воскресенье), без учета праздников
2. Дать конкретному пользователю права рута

Создадим тестовых пользователей, сразу предоставим пользователю admin полный доступ к системе без пароля
```
useradd admin 
useradd user
echo "Otus2019" | passwd --stdin admin
echo "Otus2019" | passwd --stdin user

visudo -f /etc/sudoers.d/admin
%admin        ALL=(ALL)       NOPASSWD: ALL
```

Первое задание посложнее воспользуемся pam модулем pam_exec, напишем скрипт авторизации.
Решил делать на login, а не на ssh, добавим в `/etc/pam.d/login` строчку
`account    required     pam_exec.so /usr/local/bin/test_login.sh`

Скрипт же выглядит так, проверил работает
```
#!/bin/bash

DAY=$(date "+%u")
GUID=$(getent group | grep "^admin" | awk -F ":" '{print $3}')

if [ -z "$(id $PAM_USER | grep $GUID)" ];
    then

        if [ $DAY -gt 5 ];
                then exit 1
                else exit 0
        fi
fi
```